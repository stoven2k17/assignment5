#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, 
         {'title': 'Algorithm Design', 'id': '2'}, 
         {'title': 'Python', 'id': '3'}]

@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_title = request.form['title']
        if new_title:
            all_ids = []
            for book in books:
                book_id = int(book['id'])
                all_ids.append(book_id)
            all_ids = sorted(all_ids)
            new_id = None
            for i in range(1, len(all_ids) + 2):
                if i not in all_ids:
                    new_id = str(i)
                    break
            books.append({'title': new_title, 'id': new_id})
            return redirect(url_for('showBook'))
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    book = None
    for b in books:
        if b['id'] == str(book_id):
            book = b
            break
    if request.method == 'POST':
        new_title = request.form['title']
        if book is not None and new_title:
            book['title'] = new_title
            return redirect(url_for('showBook'))
    return render_template('editBook.html', book=book)

@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    book = None
    for b in books:
        if b['id'] == str(book_id):
            book = b
            break
    if request.method == 'POST' and book is not None:
        books.remove(book)
        return redirect(url_for('showBook'))
    return render_template('deleteBook.html', book=book)

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
